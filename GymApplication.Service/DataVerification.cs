﻿using GymApplication.Service;
using System;
using System.Text.RegularExpressions;

namespace GymApplication.Infrastructure
{
    public static class DataVerification // klasa zawierająca wszystkie metody umożliwiające weryfikację poprawności wprowadzanych danych 
    {
        public static bool UserNameVerification(string name) // zwraca wynik negatywny jeżeli imie nie spełnia ustalonych wymagań
        {
            Regex nameValidation = new Regex(@"[^a-żA-Ż]+"); // sprawdza czy w imieniu występuje co najmniej jeden znak nie będący literą
            if (name.Length == 0 || nameValidation.IsMatch(name))
            {
                return false;
            }
            return true;
        }

        public static bool UserSurnameVerification(string surname) // zwraca wynik negatywny jeżeli nazwisko nie spełnia ustalonych wymagań
        {
            Regex surnameValidation = new Regex(@"[^a-żA-Ż\-]+"); // sprawdza czy w nazwisku występuje co najmniej jeden znak nie będący literą, dozwolony myślnik
            if (surname.Length == 0 || surnameValidation.IsMatch(surname))
            {
                return false;
            }
            return true;
        }

        public static bool UserPeselVerification(string pesel) // zwraca wynik negatywny jeżeli pesel nie spełnia ustalonych wymagań
        {
            Regex peselValidation = new Regex(@"\b\d{11}\b"); // sprawdza czy pesel to 11 cyfr, lub czy jest pusty
            if (pesel.Length == 0 || peselValidation.IsMatch(pesel))
            {
                return true;
            }
            return false;
        }

        public static bool UserEmailVerification(string email) // zwraca wynik negatywny jeżeli email nie spełnia ustalonych wymagań
        {
            Regex emailValidation = new Regex(@"([a-zA-Z0-9.\-_%+!?]+@[a-zA-Z0-9.-]+\.[a-zA-z]{2,4})"); // sprawdza poprawną strukture maila xxx@domena.xyz, lub czy jest pusty
            if (emailValidation.IsMatch(email) || email.Length == 0)
            {
                return true;
            }
            return false;
        }

        public static bool UserPhoneVerification(string phone) // zwraca wynik negatywny jeżeli telefon nie spełnia ustalonych wymagań
        {
            Regex phoneValidation = new Regex(@"\+\d{11}\b"); // sprawdza czy numer telefonu składa się ze znaku + a następnie 11 cyf, lub czy jest pusty
            if (phoneValidation.IsMatch(phone) || phone.Length == 0)
            {
                return true;
            }
            return false;
        }

        public static bool UserEmailPeselVerification(string email, string pesel) // jeżeli przy dodawaniu/modyfikacji danych użytkownika pesel i email są puste zwróc wynik negatywny ( chociaż jedno pole musi być wypełnione )
        {
            if (email.Length == 0 && pesel.Length == 0)
            {
                return false;
            }
            return true;
        }
        public static bool UserExistingPeselVerification(string pesel) // jeżeli użytkownik o podanym peselu istnieje już w bazie zwróc wynik negatywny
        {
            if (ClientService.GetClientbyPesel(pesel) == null || pesel.Length == 0) 
            {
                return true;
            }
            return false;
        }

        public static bool UserExistingEmailVerification(string email) // jeżeli użytkownik o podanym emailu istnieje już w bazie zwróc wynik negatywny
        {
            if (ClientService.GetClientbyEmail(email) == null || email.Length == 0) 
            {
                return true;
            }
            return false;
        }

        //                          SEGMENT POŚWIĘCONY WERYFIKACJI TWORZONEJ OFERTY 
        public static bool OfferNameVerification(string offerName) // jeżeli nazwa oferty jest pusta metoda daje wynik false
        {
            if (offerName.Length == 0) 
            {
                return false;
            }
            return true;
        }
        public static bool OfferPriceVerification(string offerPrice) // jeżeli cena oferty nie może być przekonwertowana na typ Decimal metoda daje wynik false
        {
            bool priceValue = Decimal.TryParse(offerPrice, out decimal result);
            if (priceValue) 
            {
                return true;
            }
            return false;
        }
        public static bool OfferEntryHoursVerification(string minEntry, string maxEntry) // jeżeli któraś z godzin wejściowych nie może byc przekonwertowana na typ Int metoda daje wynik false
        {
            bool minValue = Int32.TryParse(minEntry, out int minResult);
            bool maxValue = Int32.TryParse(maxEntry, out int maxResult);
            if(!minValue || !maxValue) 
            {
                return false;
            }
            return true;
        }
        public static bool OfferDurationVerification(string offerDuration)  // jeżeli czas trwania oferty nie może byc przekonwertowany na typ Int metoda daje wynik false 
        {
            bool durationValue = Int32.TryParse(offerDuration, out int durationResult);
            if (!durationValue) 
            {
                return false;
            }
            return true;
        }
        public static bool OfferDescriptionVerification(string offerDesc) // jeżeli opis oferty jest pusty metoda daje wynik false
        {
            if (offerDesc.Length == 0) 
            {
                return false;
            }
            return true;
        }
    }
}
