﻿using GymApplication.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace GymApplication.Infrastructure
{
    public class TicketService
    {
        public static List<Karnet> GetTickets()
        {
            List<Karnet> tickets = new List<Karnet>();

            using (SilowniaContext _context = new SilowniaContext())
            {
                foreach (var karnet in _context.Karnet)
                {
                    tickets.Add(karnet);
                }
                return tickets;
            }
        }

        public static Karnet GetTicketbyClientID(int clientID)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = _context.Karnet.FirstOrDefault(t => t.Idklienta == clientID);
                return ticket;
            }
        }

        public static void AddTicket(DateTime endDate, DateTime startDate, int offerID, int clientID)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = new Karnet();
                ticket.DataWaznosci = endDate;
                ticket.DataZakupu = startDate;
                ticket.Idoferty = offerID;
                ticket.Idklienta = clientID;

                _context.Karnet.Add(ticket);
                _context.SaveChanges();
            }
        }

        public async static void UpdateTicketStartingDate(int clientID, DateTime startDate)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = GetTicketbyClientID(clientID);
                ticket.DataZakupu = startDate;

                _context.Karnet.Update(ticket);
                await _context.SaveChangesAsync();
            }
        }

        public async static void UpdateTicketEndingDate(int clientID, DateTime endDate)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = GetTicketbyClientID(clientID);
                ticket.DataWaznosci = endDate;

                _context.Karnet.Update(ticket);
                await _context.SaveChangesAsync();
            }
        }

        public async static void UpdateTicketOfferID(int clientID, int offerID)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = GetTicketbyClientID(clientID);
                ticket.Idoferty = offerID;

                _context.Karnet.Update(ticket);
                await _context.SaveChangesAsync();
            }
        }

        public async static void UpdateTicketClientID(int clientID, int newClientID)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = GetTicketbyClientID(clientID);
                ticket.Idklienta = newClientID;

                _context.Karnet.Update(ticket);
                await _context.SaveChangesAsync();
            }
        }

        public async static void UpdateTicket(int clientID, DateTime startDate, DateTime endDate, int offerID)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = GetTicketbyClientID(clientID);
                ticket.DataZakupu = startDate;
                ticket.Idoferty = offerID;
                ticket.DataWaznosci = endDate;

                _context.Karnet.Update(ticket);
                await _context.SaveChangesAsync();
            }
        }

        public static void DeleteTicketByClientID(int clientid)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Karnet ticket = GetTicketbyClientID(clientid);
                if (ticket != null)
                {
                    _context.Karnet.Attach(ticket);
                    _context.Karnet.Remove(ticket);
                    _context.SaveChanges();
                }
            }
        }

    }
}
