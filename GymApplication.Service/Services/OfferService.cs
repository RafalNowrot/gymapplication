﻿using GymApplication.Core.Models;
using System.Collections.Generic;
using System.Linq;

namespace GymApplication.Infrastructure
{
    public class OfferService
    {
        public static List<Oferta> GetOfferts()
        {
            List<Oferta> offers = new List<Oferta>();

            using (SilowniaContext _context = new SilowniaContext())
            {
                foreach (var offert in _context.Oferta)
                {
                    offers.Add(offert);
                }
                return offers;
            }
        }

        public static Oferta GetOffer(int id)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = _context.Oferta.FirstOrDefault(o => o.Idoferty == id);

                return offer;
            }
        }

        public static int GetofferIDbyName(string offertName)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = _context.Oferta.FirstOrDefault(o => o.NazwaOferty == offertName);
                return offer.Idoferty;
            }
        }

        public static decimal GetOfferPriceByID(int id)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = _context.Oferta.FirstOrDefault(o => o.Idoferty == id);
                return offer.Cena;
            }
        }

        public static int GetOfferTimeByID(int id)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = _context.Oferta.FirstOrDefault(o => o.Idoferty == id);
                return offer.CzasTrwania;
            }
        }

        public static void AddOffer(string offerName,int price, int minEntry, int maxEntry, int time, string description)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = new Oferta();
                offer.NazwaOferty = offerName;
                offer.Cena = price;
                offer.MinGodzinaWejscia = minEntry;
                offer.MaxGodzinaWejscia = maxEntry;
                offer.CzasTrwania = time;
                offer.Opis = description;

                _context.Oferta.Add(offer);
                _context.SaveChanges();
            }
        }

        public static void UpdateOfferName(int id, string name)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = GetOffer(id);
                offer.NazwaOferty = name;
                _context.SaveChanges();
            }
        }

        public static void UpdateOfferPrice(int id, decimal price)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = GetOffer(id);
                offer.Cena = price;
                _context.SaveChanges();
            }
        }

        public static void UpdateOfferTime(int id, int time)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = GetOffer(id);
                offer.CzasTrwania = time;
                _context.SaveChanges();
            }
        }

        public static void UpdateOfferDescription(int id, string description)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = GetOffer(id);
                offer.Opis = description;
                _context.SaveChanges();
            }
        }

        public static void UpdateOfferMinEntry(int id, int minEntryHour)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = GetOffer(id);
                offer.MinGodzinaWejscia = minEntryHour;
                _context.SaveChanges();
            }
        }

        public static void UpdateOfferMaxEntry(int id, int maxEntryHour)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = GetOffer(id);
                offer.MaxGodzinaWejscia = maxEntryHour;
                _context.SaveChanges();
            }
        }

        public static void DeleteOffer(int id)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Oferta offer = GetOffer(id);
                _context.Oferta.Attach(offer);
                _context.Oferta.Remove(offer);
                _context.SaveChanges();
            }
        }

        public static void DeleteOffer(Oferta offer)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                _context.Oferta.Attach(offer);
                _context.Oferta.Remove(offer);
                _context.SaveChanges();
            }
        }
    }
}
