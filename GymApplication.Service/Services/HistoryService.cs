﻿using GymApplication.Core.Models;
using System;
using System.Collections.Generic;

namespace GymApplication.Infrastructure
{
    public class HistoryService
    {
        public static List<HistoriaWejsc> GetHistoryList()
        {
            List<HistoriaWejsc> historyList = new List<HistoriaWejsc>();

            using (SilowniaContext _context = new SilowniaContext())
            {
                foreach (var history in _context.HistoriaWejsc)
                {
                    historyList.Add(history);
                }
                return historyList;
            }
        }

        public static void AddClientEntryList(int clientID)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                HistoriaWejsc entry = new HistoriaWejsc();
                entry.Idklienta = clientID;
                entry.Data = DateTime.Now;
                _context.HistoriaWejsc.Add(entry);
                _context.SaveChanges();
            }
        }

        public static List<HistoriaWejsc> GetEntryByID(int id)
        {
            List<HistoriaWejsc> historyList = new List<HistoriaWejsc>();

            using (SilowniaContext _context = new SilowniaContext())
            {
                foreach (var history in _context.HistoriaWejsc)
                {
                    if (history.Idklienta == id)
                    {
                        historyList.Add(history);
                    }
                }
                return historyList;
            }
        }

        public static void DeleteClientEntryList(int clientId)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                List<HistoriaWejsc> clientHistory = GetEntryByID(clientId);
                if (clientHistory != null)
                {
                _context.RemoveRange(clientHistory);
                _context.SaveChanges();
                }
            }
        }
    }
}

