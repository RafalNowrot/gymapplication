﻿using GymApplication.Core.Models;
using System;
using System.Collections.Generic;

namespace GymApplication.Infrastructure
{
    public class ShoppingService
    {
        public static List<Zakup> GetShoppingHistory()
        {
            List<Zakup> history = new List<Zakup>();

            using (SilowniaContext _context = new SilowniaContext())
            {
                foreach (var histo in _context.Zakup)
                {
                    history.Add(histo);
                }
                return history;
            }
        }

        public static List<Zakup> GetClientShoppingHistory(int clientID)
        {
            List<Zakup> singleShoppingHistory = new List<Zakup>();
            using (SilowniaContext _context = new SilowniaContext())
            {
                foreach (var history in _context.Zakup)
                {
                    if (history.Idklienta == clientID)
                    {
                        singleShoppingHistory.Add(history);
                    }
                }
                return singleShoppingHistory;
            }
        }

        public static Zakup GetSINGLEClientShoppingHistory(int Id)
        {
            Zakup singleShoppingHistory = new Zakup();
            using (SilowniaContext _context = new SilowniaContext())
            {
                foreach (var history in _context.Zakup)
                {
                    if (history.Idzakupu == Id)
                    {
                        singleShoppingHistory = history;
                    }
                }
                return singleShoppingHistory;
            }
        }

        public static void AddShoppingHistory(int offerID, int clientID, DateTime dataZakupu, decimal price)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Zakup purchase = new Zakup();
                purchase.Idoferty = offerID;
                purchase.Idklienta = clientID;
                purchase.DataZakupu = dataZakupu;
                purchase.Cena = price;

                _context.Zakup.Add(purchase);
                _context.SaveChanges();
            }

        }

        public static void DeleteShoppingHistoryList(int clientId)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                List<Zakup> clientHistory = GetClientShoppingHistory(clientId);
                if (clientHistory != null)
                {
                    _context.RemoveRange(clientHistory);
                    _context.SaveChanges();
                }
            }
        }

        public static void DeleteShoppingHistory(Zakup zakup)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Zakup clientHistory = zakup;
                if (clientHistory != null)
                {
                    _context.Remove(clientHistory);
                    _context.SaveChanges();
                }
            }
        }

        public async static void UpdateShoppingOffer(int id, int offerID)
        {
           Zakup zakup = GetSINGLEClientShoppingHistory(id);
            zakup.Idoferty = offerID;

            using (SilowniaContext _context = new SilowniaContext())
            {
                _context.Zakup.Update(zakup);
                await _context.SaveChangesAsync();
            }
        }
    }
}
