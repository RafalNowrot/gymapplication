﻿using GymApplication.Core.Models;
using Microsoft.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;

namespace GymApplication.Service
{
    public class ClientService
    {

        public static List<Klient> GetClients()
        {
            List<Klient> clients = new List<Klient>();

            try
            {
                using (SilowniaContext _context = new SilowniaContext())
                {
                    foreach (var client in _context.Klient)
                    {
                        clients.Add(client);
                    }
                    return clients;
                }
            }
            catch (SqlException ex)
            {
                return clients;
            }
        }

        public static Klient GetClientbyID(int Id)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient client = _context.Klient.FirstOrDefault(c => c.Idklienta == Id);
                return client;
            }
        }

        public static List<Klient> GetListWithOneClientbyID(int Id) 
        {
            List<Klient> oneClient = new List<Klient>();
            using (SilowniaContext _context = new SilowniaContext()) 
            {
                oneClient.Add(_context.Klient.FirstOrDefault(c => c.Idklienta == Id));
            }
            return oneClient;               
        }

        public static List<Klient> GetListWithOneClientbyEmail(string email) 
        {
            List<Klient> client = new List<Klient>();
            using (SilowniaContext _context = new SilowniaContext()) 
            {
                client.Add(_context.Klient.FirstOrDefault(c => c.Email == email));
            }
            return client;
        }
        public static Klient GetClientbyPhone(string phoneNumber)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient client = _context.Klient.FirstOrDefault(c => c.NumerTelefonu == phoneNumber);
                return client;
            }
        }

        public static Klient GetClientbyPesel(string pesel) 
        {
            using (SilowniaContext _context = new SilowniaContext()) 
            {
                Klient client = _context.Klient.FirstOrDefault(c => c.Pesel == pesel);
                return client;
            }
        }
        public static Klient GetClientbyEmail(string email) 
        {
            using (SilowniaContext _context = new SilowniaContext()) 
            {
                Klient client = _context.Klient.FirstOrDefault(c => c.Email == email);
                return client;
            }
        }
        public static Klient GetClient(string name, string surname)
        {
            List<Klient> clients = GetClients().Where(e => e.Nazwisko == surname).ToList();
            Klient client = new Klient();
            client = (from c in clients
                      where c.Imie == name 
                      select c).FirstOrDefault();

            return client;
        }

        public static void AddClient(string name, string surname, string phoneNumber, string pesel, string email)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient klient = new Klient();
                klient.Imie = name;
                klient.Nazwisko = surname;
                klient.NumerTelefonu = phoneNumber;
                klient.Pesel = pesel;
                klient.Email = email;

                _context.Klient.Add(klient);
                _context.SaveChanges();
            }
        }

        public async static void UpdateClientNameByID(int id, string name)
        {
                Klient klient = GetClientbyID(id);
                klient.Imie = name;
            using (SilowniaContext _context = new SilowniaContext())
            {
                _context.Klient.Update(klient);
               await _context.SaveChangesAsync();
            }
        }

        public async static void UpdateClientSurnameByID(int id, string surname)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient klient = GetClientbyID(id);
                klient.Nazwisko = surname;
                _context.Klient.Update(klient);
                await _context.SaveChangesAsync();
            }
        }

        public async static void UpdateClientPeselByID(int id, string pesel)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient klient = GetClientbyID(id);
                klient.Pesel = pesel;
                _context.Klient.Update(klient);
                await _context.SaveChangesAsync();
            }
        }
        public async static void UpdateClientPhoneNumberByID(int id, string number)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient klient = GetClientbyID(id);
                klient.NumerTelefonu = number;
                _context.Klient.Update(klient);
                await _context.SaveChangesAsync();
            }
        }

        public async static void UpdateClientEmailByID(int id, string email)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient klient = GetClientbyID(id);
                klient.Email = email;
                _context.Klient.Update(klient);
                await _context.SaveChangesAsync();
            }
        }

        public static void DeleteClient(int id)
        {
            using (SilowniaContext _context = new SilowniaContext())
            {
                Klient client = GetClientbyID(id);
                _context.Klient.Remove(client);
                _context.SaveChanges();
            }
        }
    }
}
