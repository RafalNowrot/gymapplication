﻿using GymApplication.Core.Models;
using System;

namespace GymApplication.Infrastructure
{
    public class Converter
    {
        public static Klient ConvertToClient(Object obj) // konwersja potrzebna do zamienienia informacji o kliencie z wykazu klientów do typu Klient
        {
            var client = obj as Klient;
            return client;
        }


        public static Oferta ConvertToOferta(Object obj) // konwersja potrzebna do zamienienia informacji o ofercie z wykazu oferty do typu Oferta
        {
            var oferta = obj as Oferta;
            return oferta;
        }
    }
}
