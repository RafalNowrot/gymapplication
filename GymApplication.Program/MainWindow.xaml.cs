﻿using GymApplication.Infrastructure;
using GymApplication.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GymApplication.Program
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_Klient_Click(object sender, RoutedEventArgs e)
        {

            WejścieKlienta wk = new WejścieKlienta();
            wk.ShowDialog();
           
        }

        private void btn_Pracownik_Click(object sender, RoutedEventArgs e)
        {
            Hasło haslo = new Hasło();
            haslo.Show();
            this.Close();
        }

        private void grid_powitanie_MouseEnter(object sender, MouseEventArgs e)
        {
            grid_powitanie.Visibility = Visibility.Hidden;
            grid_start.Visibility = Visibility.Visible;
            grid_MenuStart.Visibility = Visibility.Visible;
        }

        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
               btn_max.ToolTip="Przywróć";
            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }
        }
    }
}
