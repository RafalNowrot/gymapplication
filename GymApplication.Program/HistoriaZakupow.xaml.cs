﻿using GymApplication.Core.Models;
using GymApplication.Infrastructure;
using GymApplication.Service;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy HistoriaZakupow.xaml
    /// </summary>
    public partial class HistoriaZakupow : Window
    {
        public HistoriaZakupow()
        {
            InitializeComponent();
            lstZakupy.ItemsSource = ShoppingService.GetShoppingHistory();
        }

        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";
            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }
        }

        private void btn_wyszukajKlienta_Click(object sender, RoutedEventArgs e)
        {
            Wyszukiwarka search = new Wyszukiwarka();
            search.Show();
        }

        private void menu_add_Click(object sender, RoutedEventArgs e)
        {
            WykazKlientów k = new WykazKlientów();
            k.Show();
        }

        private void menu_home_Click(object sender, RoutedEventArgs e)
        {
            PanelPracownika panel = new PanelPracownika();
            panel.Show();

        }

        private void menu_history_Click(object sender, RoutedEventArgs e)
        {
            Historia wejscia = new Historia();
            wejscia.Show();
        }

        private void btn_pokazZakupy_Click(object sender, RoutedEventArgs e)
        {
            Klient client = ClientService.GetClientbyEmail(txt_email.Text);
            if (client == null)
            {
                MessageBox.Show("Nie znaleziono klienta z podanym adresem email");
            }
            else
            {
                lstZakupy.ItemsSource = ShoppingService.GetClientShoppingHistory(client.Idklienta);
            }
        }

        private void btn_odswiez_Click(object sender, RoutedEventArgs e)
        {
            lstZakupy.ItemsSource = ShoppingService.GetShoppingHistory();
        }

    }
}
