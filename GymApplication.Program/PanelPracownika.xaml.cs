﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy PanelPracownika.xaml
    /// </summary>
    public partial class PanelPracownika : Window
    {
        public PanelPracownika()
        {
            InitializeComponent();
        }

        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
                grid_srodkoweKarnet.Margin = new Thickness(50, 0, 50, 0);

            }
            else
            {
                this.WindowState = WindowState.Maximized;
                grid_srodkoweKarnet.Margin = new Thickness(100,0,100,0);
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";

            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }

        }

        private void btn_WykazKlientów_Click(object sender, RoutedEventArgs e)
        {
            WykazKlientów wszyscy = new WykazKlientów();
            wszyscy.Show();
        }

        private void btn_wyszukaj_Click(object sender, RoutedEventArgs e)
        {
            Wyszukiwarka w = new Wyszukiwarka();
            w.Show();
        }

        private void btn_dodaj_Click(object sender, RoutedEventArgs e)
        {
            DodawanieKlienta nowy = new DodawanieKlienta();
            nowy.Show();
        }

        private void btn_wejscia_Click(object sender, RoutedEventArgs e)
        {
            Historia w = new Historia();
            w.Show();
        }

        private void btn_wyswietlhistorieZakupow_Click(object sender, RoutedEventArgs e)
        {
            HistoriaZakupow zakupy = new HistoriaZakupow();
            zakupy.Show();
        }

        private void btn_ofertaSilowni_Click(object sender, RoutedEventArgs e)
        {
            OfertaSilowni oferta = new OfertaSilowni();
            oferta.Show();
        }

        private void btn_Klienci_Click(object sender, RoutedEventArgs e)
        {
            WykazKlientów wykaz = new WykazKlientów();
            wykaz.Show();
        }
    }
}
