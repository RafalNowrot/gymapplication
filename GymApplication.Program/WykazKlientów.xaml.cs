﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymApplication.Service;
using System.ComponentModel;
using System.Collections;
using Microsoft.VisualBasic;
using GymApplication.Core.Models;
using GymApplication.Infrastructure;
using System.Linq;


namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy WykazKlientów.xaml
    /// </summary>
    public partial class WykazKlientów : Window
    {
        public WykazKlientów()
        {
            InitializeComponent();
            WyswietlKlientow();
        }

        public WykazKlientów(int id) 
        {
            InitializeComponent();
            WyswietlKlientow(id);
        }

        public WykazKlientów(string email)
        {
            InitializeComponent();
            WyswietlKlientow(email);
        }


        private void WyswietlKlientow()
        {
            lstKlienci.ItemsSource = ClientService.GetClients();
        }

        private void WyswietlKlientow(int clientID)
        {           
            lstKlienci.ItemsSource = ClientService.GetListWithOneClientbyID(clientID);       
        }

        private void WyswietlKlientow(string email)
        {
            lstKlienci.ItemsSource = ClientService.GetListWithOneClientbyEmail(email);
        }


        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";
            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }
        }

        private void btn_karnet_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txt_IDKlienta.Text))
            {
                MessageBox.Show("Nie wybrano klienta");
            }
            else
            {
            Karnety k = new Karnety(this);
            k.Owner = this;
            k.Show();
            }
        }

        private void btn_wyszukajKlienta_Click(object sender, RoutedEventArgs e)
        {
            Wyszukiwarka search = new Wyszukiwarka();
            search.Show();
        }

        private void menu_add_Click(object sender, RoutedEventArgs e)
        {
            DodawanieKlienta add = new DodawanieKlienta();
            add.Show();
        }

        private void btn_UsuńKlienta_Click(object sender, RoutedEventArgs e) //brakuje opcji, że aktywne tylko wtedy gdy jest wybrany rekord z bazy
        {
            MessageBoxResult r = MessageBox.Show("Czy chcesz usunąć klienta?", "Potwierdzenie", MessageBoxButton.YesNo);
            switch (r)
            {
                case MessageBoxResult.Yes:
                    MessageBoxResult potwierdzenie = MessageBox.Show("Czy na pewno?", "Potwierdzenie", MessageBoxButton.YesNo);
                    if (potwierdzenie == MessageBoxResult.Yes && txt_IDKlienta.Text != "")
                    {
                        ShoppingService.DeleteShoppingHistoryList(Convert.ToInt32(txt_IDKlienta.Text));
                        TicketService.DeleteTicketByClientID(Convert.ToInt32(txt_IDKlienta.Text));
                        HistoryService.DeleteClientEntryList(Convert.ToInt32(txt_IDKlienta.Text));
                        ClientService.DeleteClient(Convert.ToInt32(txt_IDKlienta.Text));
                    }
                    else if (potwierdzenie == MessageBoxResult.Yes && txt_IDKlienta.Text == "")
                    {
                        MessageBox.Show("Nie zaznaczono klienta do usunięcia");
                    }
                    WyswietlKlientow();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }

        private void btn_historia_Click(object sender, RoutedEventArgs e)
        {
            Historia wejscia = new Historia();
            wejscia.Show();
        }

        private void menu_history_Click(object sender, RoutedEventArgs e)
        {
            Historia wejscia = new Historia();
            wejscia.Show();
        }

  
        private void lstKlienci_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {

            Klient client = Converter.ConvertToClient(lstKlienci.SelectedItem);

            if (client != null)
            {
                txt_IDKlienta.Text = client.Idklienta.ToString();
                txt_imie.Text = client.Imie.ToString();
                txt_nazwisko.Text = client.Nazwisko.ToString();
                txt_pesel.Text = client.Pesel.ToString();
                txt_mail.Text = client.Email.ToString();
                txt_telefon.Text = client.NumerTelefonu.ToString();
                
            }
            else MessageBox.Show("Nie zaznaczono klienta");
        }

        private void btn_modyfikujDane_Click(object sender, RoutedEventArgs e)
        {
            if (string.IsNullOrEmpty(txt_IDKlienta.Text))
            {
                MessageBox.Show("Nie wybrano klienta");
            }
            else
            {
                EdycjaKlienta edycja = new EdycjaKlienta(this);
                edycja.Owner = this;
                edycja.ShowDialog();
                WyswietlKlientow();
               
            }
        }

        private void btn_odswiez_Click(object sender, RoutedEventArgs e)
        {
            WyswietlKlientow();
        }

        private void menu_home_Click(object sender, RoutedEventArgs e)
        {
            PanelPracownika panel = new PanelPracownika();
            panel.Show();
        }

        private void cmb_sortowanie_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            IEnumerable<Klient> orderedByName = ClientService.GetClients().OrderBy(c => c.Imie);
            IEnumerable<Klient> orderedBySurname = ClientService.GetClients().OrderBy(c => c.Nazwisko);
            IEnumerable<Klient> orderedByPesel = ClientService.GetClients().OrderBy(c => c.Pesel);
            IEnumerable<Klient> orderedByEmail = ClientService.GetClients().OrderBy(c => c.Email);
            IEnumerable<Klient> orderedByPhone = ClientService.GetClients().OrderBy(c => c.NumerTelefonu);

            if (lstKlienci != null)
            {
                if (cmb_sortowanie.SelectedIndex == 0)
                {
                    lstKlienci.ItemsSource = ClientService.GetClients();
                }
                else if (cmb_sortowanie.SelectedIndex == 1)
                {
                    lstKlienci.ItemsSource = orderedByName;
                }
                else if (cmb_sortowanie.SelectedIndex == 2)
                {
                    lstKlienci.ItemsSource = orderedBySurname;
                }
                else if (cmb_sortowanie.SelectedIndex == 3)
                {
                    lstKlienci.ItemsSource = orderedByPesel;
                }
                else if (cmb_sortowanie.SelectedIndex == 4)
                {
                    lstKlienci.ItemsSource = orderedByEmail;
                }
                else if (cmb_sortowanie.SelectedIndex == 5)
                {
                    lstKlienci.ItemsSource = orderedByPhone;
                }
            }
        }
    }
}
    
     
