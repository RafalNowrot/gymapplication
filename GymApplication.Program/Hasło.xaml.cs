﻿using GymApplication.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy Hasło.xaml
    /// </summary>
    public partial class Hasło : Window
    {
        public Hasło()
        {
            InitializeComponent();
        }

        private void btn_Zaloguj_Click(object sender, RoutedEventArgs e)
        {
            Autorisation autorisation = new Autorisation();
            if (password_haslo.Password == autorisation.Haslo)
            {
                PanelPracownika panel = new PanelPracownika();
                panel.Show();
               
                this.Close();
            }
            else MessageBox.Show("Podano niepoprawne hasło");
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
            MainWindow main = new MainWindow();
            main.Show();
        }
    }
}
