﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymApplication.Core.Models;
using GymApplication.Infrastructure;
using GymApplication.Service;




namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy Historia.xaml
    /// </summary>
    public partial class Historia : Window
    {
        public Historia()
        {
            InitializeComponent();
            WyswietlHistorie();
            this.SizeToContent = SizeToContent.Width;
        }
        private void WyswietlHistorie()
        {
            lstHistoriaWejsc.ItemsSource = HistoryService.GetHistoryList();
        }
        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
             this.DragMove();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                this.SizeToContent = SizeToContent.Width;
                WindowState = WindowState.Normal;
            }
            else
            {
                this.SizeToContent = SizeToContent.Manual;
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";
            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }

        }

        private void btn_wyszukajKlienta_Click(object sender, RoutedEventArgs e)
        {
            btn_wyszukajKlienta.Visibility = Visibility.Hidden;
            grid_WyszukiwanieKlienta.Width = 500;         
            grid_WyszukiwanieKlienta.Visibility = Visibility.Visible;
        }

        private void btn_wyszukaj_Click(object sender, RoutedEventArgs e)
        {
            if (radbtn_idKlienta.IsChecked == true)
            {
                this.SizeToContent = SizeToContent.Manual;
                this.WindowState = WindowState.Maximized;
                grid_wyszukanyKlient.Width = 550;
                grid_wyszukanyKlient.HorizontalAlignment = HorizontalAlignment.Stretch;
                grid_wyszukanyKlient.VerticalAlignment = VerticalAlignment.Center;
                grid_WyszukiwanieKlienta.VerticalAlignment = VerticalAlignment.Center;
                grid_wyszukanyKlient.Visibility = Visibility.Visible;
                lst_Wejscwyszukanego.ItemsSource = HistoryService.GetEntryByID(Convert.ToInt32(txt_IDKlienta.Text));
                Klient wyszukanyKlient = ClientService.GetClientbyID(Convert.ToInt32(txt_IDKlienta.Text));
                if (wyszukanyKlient != null)
                {
                    txt_idklientaWyszukanego.Text = wyszukanyKlient.Idklienta.ToString();
                    txt_imieWyszukanego.Text = wyszukanyKlient.Imie;
                    txt_nazwiskoWyszukanego.Text = wyszukanyKlient.Nazwisko;
                    txt_mailWyszukanego.Text = wyszukanyKlient.Email;
                    txt_peselWyszukanego.Text = wyszukanyKlient.Pesel;
                    txt_telefonWyszukanego.Text = wyszukanyKlient.NumerTelefonu;
                }
            }
            if (radbtn_imieNazwisko.IsChecked == true || radbtn_telefon.IsChecked == true) 
            {
                MessageBox.Show("Opcja jeszcze niedostępna");
            }
        }

        private void btn_Anuluj_Click(object sender, RoutedEventArgs e)
        {
            btn_wyszukajKlienta.Visibility = Visibility.Visible;
            grid_wyszukanyKlient.Visibility = Visibility.Hidden;
            grid_WyszukiwanieKlienta.Visibility = Visibility.Hidden;
            grid_WyszukiwanieKlienta.Width = 50;
            grid_wyszukanyKlient.Width = 50;
            this.WindowState = WindowState.Normal;
            this.SizeToContent = SizeToContent.WidthAndHeight;
        }

        private void menu_home_Click(object sender, RoutedEventArgs e)
        {
            PanelPracownika p = new PanelPracownika();
            p.Show();
        }

        private void menu_wykaz_Click(object sender, RoutedEventArgs e)
        {
            WykazKlientów w = new WykazKlientów();
            w.Show();
        }

        private void menu_oferta_Click(object sender, RoutedEventArgs e)
        {
            OfertaSilowni oferta = new OfertaSilowni();
            oferta.Show();
        }

        private void menu_history_Click(object sender, RoutedEventArgs e)
        {
            HistoriaZakupow zak = new HistoriaZakupow();
            zak.Show();
        }
    }
}
