﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymApplication.Core.Models;
using GymApplication.Infrastructure;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy WejścieKlienta.xaml
    /// </summary>
    public partial class WejścieKlienta : Window
    {
        public WejścieKlienta()
        {
            InitializeComponent();
        }

        private void btn_dodajWejscie_Click(object sender, RoutedEventArgs e)
        {

            var karnety = TicketService.GetTickets();
            foreach (Karnet karnet in karnety) // sprawdzenie czy skanowane id karnetu znajduje się w bazie danych
            {
                if (karnet.Idkarnetu == Convert.ToInt32(txt_IDKarnetu.Text))
                {
                    Oferta oferta = OfferService.GetOffer(karnet.Idoferty);
                    if (oferta.MinGodzinaWejscia > DateTime.Now.Hour) // sprawdzenie czy godzina wejscia nie jest za wczesna co do oferty
                    {
                        MessageBox.Show($"Jesteś za wcześnie, możesz wejść na siłownię od {oferta.MinGodzinaWejscia}");
                        break;
                    }
                    if (oferta.MaxGodzinaWejscia < DateTime.Now.Hour)  // sprawdzenie czy godzina wejscia nie jest za późna co do oferty
                    {
                        MessageBox.Show($"Jesteś za późno, możesz wejść na siłownię do {oferta.MaxGodzinaWejscia}"); 
                        break;
                    }
                    if (karnet.DataWaznosci > DateTime.Today.AddHours(23).AddMinutes(59))  // sprawdzenie czy data ważności karnetu dalej obowiązuje 
                    {
                       
                        MessageBox.Show($"Karnet był ważny do {karnet.DataWaznosci}");
                        
                    }
                    HistoryService.AddClientEntryList(karnet.Idklienta);
                    MessageBox.Show("Wejście zostało zapisane!", DateTime.Now.ToString(), MessageBoxButton.OK);
                    break;
                }
            }
            this.Close();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
