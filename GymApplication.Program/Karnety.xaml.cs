﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymApplication.Service;
using GymApplication.Infrastructure;
using GymApplication.Core.Models;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy Karnety.xaml
    /// </summary>
    public partial class Karnety : Window
    {
        WykazKlientów wykaz;

        private Karnet karnetWybranegoKlienta;
        private Oferta wybranaOferta;
        public Karnety(WykazKlientów wykaz)
        {
            InitializeComponent();
            this.SizeToContent = SizeToContent.Width;

            this.wykaz = wykaz;
            InitializeComponent();
            txt_imie.Text = wykaz.txt_imie.Text;
            txt_nazwisko.Text = wykaz.txt_nazwisko.Text;
            txt_mail.Text = wykaz.txt_mail.Text;
            txt_pesel.Text = wykaz.txt_pesel.Text;
            txt_telefon.Text = wykaz.txt_telefon.Text;
            txt_idklienta.Text = wykaz.txt_IDKlienta.Text;
            txt_idklientaKarnet.Text = wykaz.txt_IDKlienta.Text;
            if (TicketService.GetTicketbyClientID(Convert.ToInt32(txt_idklienta.Text)) != null) 
            {
                Karnet karnet = TicketService.GetTicketbyClientID(Convert.ToInt32(txt_idklienta.Text));
                karnetWybranegoKlienta = karnet;
                txt_idKarnetu.Text = karnetWybranegoKlienta.Idkarnetu.ToString();
                txt_dataZakupu.Text = karnetWybranegoKlienta.DataZakupu.ToString();
                txt_dataWaznosci.Text = karnetWybranegoKlienta.DataWaznosci.ToString();
                txt_idOferty.Text = karnetWybranegoKlienta.Idoferty.ToString();
            }
            
            WyswietlHistorie();
            WyswietlOferte();
        }

        private void WyswietlOferte() 
        {
            lst_Oferta.ItemsSource = OfferService.GetOfferts();    
        }
        private void WyswietlHistorie()
        {
            List<HistoriaWejsc> historiaWejscs = HistoryService.GetEntryByID(Convert.ToInt32(txt_idklienta.Text));
            if (historiaWejscs != null)
            {
                lst_WejscwyszukanegoWkarnetach.ItemsSource = historiaWejscs;
            }
        }
        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                this.SizeToContent = SizeToContent.Width;
                WindowState = WindowState.Normal;
            }
            else
            {
                this.SizeToContent = SizeToContent.Manual;
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";

            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }
        }

        private void menu_home_Click(object sender, RoutedEventArgs e)
        {
            PanelPracownika p = new PanelPracownika();
            p.Show();
        }

        
        private void menu_wykaz_Click(object sender, RoutedEventArgs e)
        {
            WykazKlientów w = new WykazKlientów();
            w.Show();
        }

        private void menu_history_Click(object sender, RoutedEventArgs e)
        {
            Historia historiawejsc = new Historia();
            historiawejsc.Show();
        }

        private void btn_zakupKarnetu_Click(object sender, RoutedEventArgs e)
        {
           
           if (btn_zakupKarnetu.Content.ToString() == "Rozpocznij zmiany")
           { 
                kalendarz.BlackoutDates.AddDatesInPast();
                txt_dataZakupu.IsEnabled = true;
                txt_idOferty.IsEnabled = true;
                txt_dataZakupu.Background = Brushes.DarkGray;
                txt_idOferty.Background = Brushes.DarkGray;
                btn_zakupKarnetu.Content = "Zapisz zmiany";     
           }
            else
            {
                txt_dataZakupu.IsEnabled = false;
                txt_idOferty.IsEnabled = false;
                txt_dataZakupu.Background = Brushes.Transparent;
                txt_idOferty.Background = Brushes.Transparent;
                btn_zakupKarnetu.Content = "Rozpocznij zmiany";
                if (kalendarz.SelectedDate.HasValue)
                {
                    double daysToAdd = Convert.ToDouble(wybranaOferta.CzasTrwania);
                    DateTime dataRozpoczecia = kalendarz.SelectedDate.Value.AddHours(0).AddMinutes(0);
                    DateTime dataWaznosci = dataRozpoczecia.AddDays(daysToAdd).AddHours(23).AddMinutes(59);
                    txt_dataWaznosci.Text = dataWaznosci.ToString();
                    if (string.IsNullOrEmpty(txt_idKarnetu.Text)) 
                    {
                        TicketService.AddTicket(dataWaznosci, dataRozpoczecia, wybranaOferta.Idoferty, Convert.ToInt32(txt_idklienta.Text));
                    }
                    else 
                    {
                        TicketService.UpdateTicket(Convert.ToInt32(txt_idklienta.Text), dataRozpoczecia, dataWaznosci, Convert.ToInt32(txt_idOferty.Text));
                    }
                    ShoppingService.AddShoppingHistory(Convert.ToInt32(txt_idOferty.Text), Convert.ToInt32(txt_idklienta.Text), DateTime.Now, wybranaOferta.Cena);
                }
                else 
                {
                    MessageBox.Show("Wybierz datę rozpoczęcia karnetu");
                }
            }
        }

        private void lstOferta_MouseDoubleClick(object sender, RoutedEventArgs e) 
        {
            Oferta oferta = Converter.ConvertToOferta(lst_Oferta.SelectedItem);
            if (oferta != null) 
            {
                wybranaOferta = oferta;
                txt_idOferty.Text = wybranaOferta.Idoferty.ToString();
                txt_idklientaKarnet.Text = txt_idklienta.Text;
            }
            
        }

    }
}
