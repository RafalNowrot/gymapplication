﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymApplication.Service;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy Wyszukiwarka.xaml
    /// </summary>
    public partial class Wyszukiwarka : Window
    {
        public Wyszukiwarka()
        {
            InitializeComponent();
        }

        private void btn_wyszukaj_Click(object sender, RoutedEventArgs e)
        {
            if (radbtn_idKlienta.IsChecked == true )
            {
                {
                    if (string.IsNullOrEmpty(txt_IDKlienta.Text))
                    {
                        MessageBox.Show("Podaj ID, żeby wyszukać klienta");
                    }
                    else
                    {
                        //ClientService.GetClientbyID(Convert.ToInt32(txt_IDKlienta.Text));
                        WykazKlientów wykaz = new WykazKlientów(Convert.ToInt32(txt_IDKlienta.Text));
                        wykaz.Show();
                        this.Close();
                    }
                }


            }
            else if (radbtn_imieNazwisko.IsChecked == true)
            {
                radbtn_imieNazwisko.IsChecked = false;
                radbtn_idKlienta.IsChecked = true;
            }
            else if (radbtn_email.IsChecked == true)
            {

                WykazKlientów wykaz = new WykazKlientów(txt_email.Text);
                wykaz.Show();
                this.Close();
            }
            else
                this.Close();
        }

        private void btn_Anuluj_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void radbtn_imieNazwisko_Checked(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Opcja chwilowo niedostępna");
        }
    }
}
