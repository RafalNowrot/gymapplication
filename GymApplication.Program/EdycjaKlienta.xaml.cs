﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymApplication.Infrastructure;
using GymApplication.Service;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy EdycjaKlienta.xaml
    /// </summary>
    public partial class EdycjaKlienta : Window
    {
        WykazKlientów wykaz;
        public EdycjaKlienta( WykazKlientów wykaz)
        {
            this.wykaz = wykaz;
            InitializeComponent();
            txt_imie.Text = wykaz.txt_imie.Text;
            txt_nazwisko.Text = wykaz.txt_nazwisko.Text;
            txt_mail.Text = wykaz.txt_mail.Text;
            txt_pesel.Text = wykaz.txt_pesel.Text;
            txt_telefon.Text = wykaz.txt_telefon.Text;
            txt_id.Text = wykaz.txt_IDKlienta.Text;
        }
        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }
        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("Czy chcesz zakończyć bez zmiany danych klienta?", "Potwierdzenie", MessageBoxButton.YesNo);
            switch (r)
            {
                case MessageBoxResult.Yes:
                    MessageBoxResult potwierdzenie = MessageBox.Show("Czy na pewno?", "Potwierdzenie", MessageBoxButton.YesNo);
                    if (potwierdzenie == MessageBoxResult.Yes)
                    {
                        this.Close();
                    }

                    break;
                case MessageBoxResult.No:

                    break;
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";
            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }
        }

        private void btn_Zmien_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult rezultat = MessageBox.Show("Czy chcesz zmienić dane klienta?", "Potwierdzenie", MessageBoxButton.YesNoCancel);
            switch (rezultat)
            {
                case MessageBoxResult.Yes:
                    if ( chbx_imie.IsChecked == true && DataVerification.UserNameVerification(txt_imie.Text))
                    {
                        ClientService.UpdateClientNameByID(Convert.ToInt32(txt_id.Text), txt_imie.Text);
                        MessageBox.Show("Dane klienta zostawy zmodyfikowane", "Potwierdzenie");
                        this.Close();
                    }
                    if (chbx_nazwisko.IsChecked == true && DataVerification.UserSurnameVerification(txt_nazwisko.Text))
                    {
                        ClientService.UpdateClientSurnameByID(Convert.ToInt32(txt_id.Text), txt_nazwisko.Text);
                        MessageBox.Show("Dane klienta zostawy zmodyfikowane", "Potwierdzenie");
                        this.Close();
                    }
                    if (chbx_pesel.IsChecked == true && DataVerification.UserPeselVerification(txt_pesel.Text) && DataVerification.UserExistingPeselVerification(txt_pesel.Text) && DataVerification.UserEmailPeselVerification(txt_mail.Text,txt_pesel.Text))
                    {
                        ClientService.UpdateClientPeselByID(Convert.ToInt32(txt_id.Text), txt_pesel.Text);
                        MessageBox.Show("Dane klienta zostawy zmodyfikowane", "Potwierdzenie");
                        this.Close();
                    }
                    
                    if (chbx_mail.IsChecked == true && DataVerification.UserEmailVerification(txt_mail.Text) && DataVerification.UserExistingEmailVerification(txt_mail.Text) && DataVerification.UserEmailPeselVerification(txt_mail.Text, txt_pesel.Text))
                    {
                        ClientService.UpdateClientEmailByID(Convert.ToInt32(txt_id.Text), txt_mail.Text);
                        MessageBox.Show("Dane klienta zostawy zmodyfikowane", "Potwierdzenie");
                        this.Close();
                    }
                    if (chbx_telefon.IsChecked == true && DataVerification.UserPhoneVerification(txt_telefon.Text))
                    {
                        ClientService.UpdateClientPhoneNumberByID(Convert.ToInt32(txt_id.Text), txt_telefon.Text);
                        MessageBox.Show("Dane klienta zostawy zmodyfikowane", "Potwierdzenie");
                        this.Close();
                    }
                    
                    break;
                case MessageBoxResult.No:

                    MessageBoxResult potwierdzenie = MessageBox.Show("Czy na pewno?", "Potwierdzenie", MessageBoxButton.YesNo);
                    if (potwierdzenie == MessageBoxResult.Yes)
                    {
                        this.Close();
                    }

                    break;
                case MessageBoxResult.Cancel:
                    break;
            }
        }

        private void btn_Anuluj_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("Czy chcesz zakończyć bez zmiany danych klienta?", "Potwierdzenie", MessageBoxButton.YesNo);
            switch (r)
            {
                case MessageBoxResult.Yes:
                    MessageBoxResult potwierdzenie = MessageBox.Show("Czy na pewno?", "Potwierdzenie", MessageBoxButton.YesNo);
                    if (potwierdzenie == MessageBoxResult.Yes)
                    {
                        this.Close();
                    }

                    break;
                case MessageBoxResult.No:
                    break;
            }
        } 
    }
}