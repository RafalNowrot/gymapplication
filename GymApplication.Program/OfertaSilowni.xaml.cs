﻿using GymApplication.Core.Models;
using GymApplication.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy Oferta.xaml
    /// </summary>
    public partial class OfertaSilowni : Window
    {
        public OfertaSilowni()
        {
            InitializeComponent();
            WyswietlOferte();
        }

        private void WyswietlOferte() 
        {
            lstOfert.ItemsSource = OfferService.GetOfferts();
        }

        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {


            this.DragMove();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;

            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";

            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }

        }

        

        private void menu_home_Click(object sender, RoutedEventArgs e)
        {
            PanelPracownika p = new PanelPracownika();
            p.Show();
            this.Close();
        }

        private void menu_history_Click(object sender, RoutedEventArgs e)
        {
            Historia wejscia = new Historia();
            wejscia.Show();
        }

        private void menu_historiaZakupow_Click(object sender, RoutedEventArgs e)
        {
            HistoriaZakupow zakupy = new HistoriaZakupow();
            zakupy.Show();
        }

        private void btn_odswiez_Click(object sender, RoutedEventArgs e)
        {
            WyswietlOferte();
        }

        private void lstOfert_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Oferta oferta = Converter.ConvertToOferta(lstOfert.SelectedItem);

            if (oferta != null) 
            {
                txt_IDOferty.Text = oferta.Idoferty.ToString();
                txt_cena.Text = oferta.Cena.ToString();
                txt_CzasTrwania.Text = oferta.CzasTrwania.ToString();
                txt_minGodz.Text = oferta.MinGodzinaWejscia.ToString();
                txt_maxGodz.Text = oferta.MaxGodzinaWejscia.ToString();
                txt_NazwaOferty.Text = oferta.NazwaOferty.ToString();
                txt_opis.Text = oferta.Opis.ToString();
            }
        }

        private void btn_dodawanieOferty_Click(object sender, RoutedEventArgs e)
        {

            btn_zakonczenieDodawaniaOferty.Visibility = Visibility.Visible;
            txt_NazwaOferty.IsEnabled = true;
            txt_cena.IsEnabled = true;
            txt_minGodz.IsEnabled = true;
            txt_maxGodz.IsEnabled = true;
            txt_CzasTrwania.IsEnabled = true;
            txt_opis.IsEnabled = true;
        }

        private void btn_zakonczenieDodawaniaOferty_Click(object sender, RoutedEventArgs e)
        {


            MessageBoxResult rezultat = MessageBox.Show("Czy chcesz dodać nową ofertę ?", "Potwierdzenie", MessageBoxButton.YesNoCancel);
            switch (rezultat)
            {
                case MessageBoxResult.Yes:
                    if (!DataVerification.OfferNameVerification(txt_NazwaOferty.Text))
                    {
                        MessageBox.Show("Nazwa oferty nie może być pusta");
                        break;
                    }
                    if (!DataVerification.OfferPriceVerification(txt_cena.Text))
                    {
                        MessageBox.Show("Wprowadzona cena jest w złym formacie lub pole jest puste");
                        break;
                    }
                    if (!DataVerification.OfferEntryHoursVerification(txt_minGodz.Text, txt_maxGodz.Text))
                    {
                        MessageBox.Show("Któraś z godzin wejściowych jest w złym formacie");
                        break;
                    }
                    if (!DataVerification.OfferDurationVerification(txt_CzasTrwania.Text))
                    {
                        MessageBox.Show("Czas trawania oferty jest w złym formacie lub pole jest puste");
                        break;
                    }
                    if (!DataVerification.OfferDescriptionVerification(txt_opis.Text))
                    {
                        MessageBox.Show("Opis oferty nie może być pusty");
                        break;
                    }
                    OfferService.AddOffer(txt_NazwaOferty.Text, Convert.ToInt32(txt_cena.Text), Convert.ToInt32(txt_minGodz.Text), Convert.ToInt32(txt_maxGodz.Text), Convert.ToInt32(txt_CzasTrwania.Text), txt_opis.Text);
                    btn_zakonczenieDodawaniaOferty.Visibility = Visibility.Hidden;
                    txt_NazwaOferty.IsEnabled = false;
                    txt_cena.IsEnabled = false;
                    txt_minGodz.IsEnabled = false;
                    txt_maxGodz.IsEnabled = false;
                    txt_CzasTrwania.IsEnabled = false;
                    txt_opis.IsEnabled = false;
                    txt_NazwaOferty.Text = "";
                    txt_cena.Text = "";
                    txt_CzasTrwania.Text = "";
                    txt_maxGodz.Text = "";
                    txt_minGodz.Text = "";
                    txt_opis.Text = "";
                    WyswietlOferte();
                    break;
            }

        }

        private void btn_UsunOferte_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("Czy chcesz usunąć ofertę", "Potwierdzenie", MessageBoxButton.YesNo);
            switch (r)
            {
                case MessageBoxResult.Yes:
                    MessageBoxResult potwierdzenie = MessageBox.Show("Czy na pewno?", "Potwierdzenie", MessageBoxButton.YesNo);
                    if (potwierdzenie == MessageBoxResult.Yes && txt_IDOferty.Text != "")
                    {
                        Oferta offer = Converter.ConvertToOferta(lstOfert.SelectedItem);
                        OfferService.DeleteOffer(offer);
                    }
                    else if (potwierdzenie == MessageBoxResult.Yes && txt_IDOferty.Text == "")
                    {
                        MessageBox.Show("Nie zaznaczono oferty do usunięcia");
                    }
                    WyswietlOferte();
                    break;
                case MessageBoxResult.No:
                    break;
            }
        }
    }
}
