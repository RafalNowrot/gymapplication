﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GymApplication.Infrastructure;
using GymApplication.Service;


namespace GymApplication.Program
{
    /// <summary>
    /// Logika interakcji dla klasy DodawanieKlienta.xaml
    /// </summary>
    public partial class DodawanieKlienta : Window
    {

        public DodawanieKlienta()
        {
            InitializeComponent();
        }

        private void grid_MenuStart_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.DragMove();
        }

        private void btn_end_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("Czy chcesz wyjść bez dodania klienta?", "Potwierdzenie", MessageBoxButton.YesNo);
            switch (r)
            {
                case MessageBoxResult.Yes:
                   
                        this.Close();
                   break;
                case MessageBoxResult.No:

                    break;
            }
            this.Close();
        }

        private void btn_min_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = WindowState.Minimized;
        }
        private void btn_max_Click(object sender, RoutedEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                this.WindowState = WindowState.Maximized;
            }
        }

        private void btn_max_MouseEnter(object sender, MouseEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                btn_max.ToolTip = "Przywróć";
            }
            else
            {
                btn_max.ToolTip = "Maksymalizuj";
            }
        }

        private void btn_Dodaj_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult rezultat = MessageBox.Show("Czy chcesz dodać nowego klienta?", "Potwierdzenie", MessageBoxButton.YesNoCancel);
            switch (rezultat)
            {
                case MessageBoxResult.Yes:
                    if (!DataVerification.UserNameVerification(txt_imie.Text))  
                    {
                        MessageBox.Show("Wprowadzono błędne imie");
                        break;
                    }
                    if (!DataVerification.UserSurnameVerification(txt_nazwisko.Text)) 
                    {
                        MessageBox.Show("Wprowadzono błędne nazwisko");
                        break;
                    }
                    if (!DataVerification.UserPeselVerification(txt_pesel.Text)) 
                    {
                        MessageBox.Show("Wprowadzono błędny pesel");
                        break;
                    }
                    if (!DataVerification.UserEmailVerification(txt_mail.Text)) 
                    {
                        MessageBox.Show("Wprowadzono błędny email");
                        break;
                    }
                    if (!DataVerification.UserPhoneVerification(txt_telefon.Text)) 
                    {
                        MessageBox.Show("Wprowadzono błędny numer telefonu");
                        break;
                    }
                    if (!DataVerification.UserEmailPeselVerification(txt_mail.Text, txt_pesel.Text)) 
                    {
                        MessageBox.Show("Pole pesel i email nie mogą być na raz puste");
                        break;
                    }
                    if (!DataVerification.UserExistingEmailVerification(txt_mail.Text)) 
                    {
                        MessageBox.Show("Podany email istnieje już w bazie");
                        break;
                    }
                    if (!DataVerification.UserExistingPeselVerification(txt_pesel.Text)) 
                    {
                        MessageBox.Show("Podany pesel istnieje jużw bazie");
                        break;
                    }
                    ClientService.AddClient(txt_imie.Text, txt_nazwisko.Text, txt_telefon.Text, txt_pesel.Text, txt_mail.Text);
                    MessageBox.Show("Klient został dodany", "Potwierdzenie");
                    this.Close();
                    break;
                case MessageBoxResult.No:
                    
                        MessageBoxResult potwierdzenie = MessageBox.Show("Czy na pewno?", "Potwierdzenie", MessageBoxButton.YesNo);
                    if (potwierdzenie == MessageBoxResult.Yes)
                    {
                        this.Close();
                    }
                   
                    break;
                case MessageBoxResult.Cancel:
                    
                    break;
            }
        }

        private void btn_Anuluj_Click(object sender, RoutedEventArgs e)
        {
            MessageBoxResult r = MessageBox.Show("Czy chcesz anulować dodawanie nowego klienta?", "Potwierdzenie", MessageBoxButton.YesNo);
            switch (r)
            {
                case MessageBoxResult.Yes:
                    MessageBoxResult potwierdzenie = MessageBox.Show("Czy na pewno?", "Potwierdzenie", MessageBoxButton.YesNo);
                    if (potwierdzenie == MessageBoxResult.Yes)
                    {
                        this.Close();
                    }
          
                   break;
                case MessageBoxResult.No:
                   
                 break;
               
            }
        }
    }
}
