﻿using System;
using System.Collections.Generic;

namespace GymApplication.Core.Models
{
    public partial class HistoriaWejsc
    {
        public int Idwejscia { get; set; }
        public int Idklienta { get; set; }
        public DateTime Data { get; set; }

        public virtual Klient IdklientaNavigation { get; set; }
    }
}
