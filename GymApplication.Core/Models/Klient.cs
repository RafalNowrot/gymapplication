﻿using System;
using System.Collections.Generic;

namespace GymApplication.Core.Models
{
    public partial class Klient
    {
        public Klient()
        {
            HistoriaWejsc = new HashSet<HistoriaWejsc>();
            Karnet = new HashSet<Karnet>();
            Zakup = new HashSet<Zakup>();
        }

        public int Idklienta { get; set; }
        public string Imie { get; set; }
        public string Nazwisko { get; set; }
        public string Pesel { get; set; }
        public string Email { get; set; }
        public string NumerTelefonu { get; set; }

        public virtual ICollection<HistoriaWejsc> HistoriaWejsc { get; set; }
        public virtual ICollection<Karnet> Karnet { get; set; }
        public virtual ICollection<Zakup> Zakup { get; set; }
    }
}
