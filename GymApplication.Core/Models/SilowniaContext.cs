﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace GymApplication.Core.Models
{
    public partial class SilowniaContext : DbContext
    {
        public SilowniaContext()
        {
        }

        public SilowniaContext(DbContextOptions<SilowniaContext> options)
            : base(options)
        {
        }

        public virtual DbSet<HistoriaWejsc> HistoriaWejsc { get; set; }
        public virtual DbSet<Karnet> Karnet { get; set; }
        public virtual DbSet<Klient> Klient { get; set; }
        public virtual DbSet<Oferta> Oferta { get; set; }
        public virtual DbSet<Zakup> Zakup { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=Silownia;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=False;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<HistoriaWejsc>(entity =>
            {
                entity.HasKey(e => e.Idwejscia)
                    .HasName("PK_dbo.HistoriaWejsc")
                    .IsClustered(false);

                entity.Property(e => e.Idwejscia).HasColumnName("IDWejscia");

                entity.Property(e => e.Data)
                    .HasColumnName("data")
                    .HasColumnType("datetime");

                entity.Property(e => e.Idklienta).HasColumnName("IDKlienta");

                entity.HasOne(d => d.IdklientaNavigation)
                    .WithMany(p => p.HistoriaWejsc)
                    .HasForeignKey(d => d.Idklienta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_dbo.HistoriaWejsc_Klient");
            });

            modelBuilder.Entity<Karnet>(entity =>
            {
                entity.HasKey(e => e.Idkarnetu)
                    .IsClustered(false);

                entity.Property(e => e.Idkarnetu).HasColumnName("IDKarnetu");

                entity.Property(e => e.DataWaznosci)
                    .HasColumnName("dataWaznosci")
                    .HasColumnType("date");

                entity.Property(e => e.DataZakupu)
                    .HasColumnName("dataZakupu")
                    .HasColumnType("date");

                entity.Property(e => e.Idklienta).HasColumnName("IDKlienta");

                entity.Property(e => e.Idoferty).HasColumnName("IDOferty");

                entity.HasOne(d => d.IdklientaNavigation)
                    .WithMany(p => p.Karnet)
                    .HasForeignKey(d => d.Idklienta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Karnet_Klient");

                entity.HasOne(d => d.IdofertyNavigation)
                    .WithMany(p => p.Karnet)
                    .HasForeignKey(d => d.Idoferty)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Karnet_Oferta");
            });

            modelBuilder.Entity<Klient>(entity =>
            {
                entity.HasKey(e => e.Idklienta)
                    .IsClustered(false);

                entity.Property(e => e.Idklienta).HasColumnName("IDKlienta");

                entity.Property(e => e.Email)
                    .HasColumnName("email")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Imie)
                    .IsRequired()
                    .HasColumnName("imie")
                    .HasMaxLength(40);

                entity.Property(e => e.Nazwisko)
                    .IsRequired()
                    .HasColumnName("nazwisko")
                    .HasMaxLength(40);

                entity.Property(e => e.NumerTelefonu)
                    .HasColumnName("numerTelefonu")
                    .HasMaxLength(12)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Pesel)
                    .HasColumnName("pesel")
                    .HasMaxLength(11)
                    .IsFixedLength();
            });

            modelBuilder.Entity<Oferta>(entity =>
            {
                entity.HasKey(e => e.Idoferty)
                    .IsClustered(false);

                entity.Property(e => e.Idoferty).HasColumnName("IDOferty");

                entity.Property(e => e.Cena)
                    .HasColumnName("cena")
                    .HasColumnType("money");

                entity.Property(e => e.CzasTrwania).HasColumnName("czasTrwania");

                entity.Property(e => e.MaxGodzinaWejscia).HasColumnName("maxGodzinaWejscia");

                entity.Property(e => e.MinGodzinaWejscia).HasColumnName("minGodzinaWejscia");

                entity.Property(e => e.NazwaOferty)
                    .IsRequired()
                    .HasColumnName("nazwaOferty")
                    .HasMaxLength(30);

                entity.Property(e => e.Opis)
                    .IsRequired()
                    .HasColumnName("opis")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<Zakup>(entity =>
            {
                entity.HasKey(e => e.Idzakupu)
                    .IsClustered(false);

                entity.Property(e => e.Idzakupu).HasColumnName("IDZakupu");

                entity.Property(e => e.Cena)
                    .HasColumnName("cena")
                    .HasColumnType("money");

                entity.Property(e => e.DataZakupu)
                    .HasColumnName("dataZakupu")
                    .HasColumnType("date");

                entity.Property(e => e.Idklienta).HasColumnName("IDKlienta");

                entity.Property(e => e.Idoferty).HasColumnName("IDOferty");

                entity.HasOne(d => d.IdklientaNavigation)
                    .WithMany(p => p.Zakup)
                    .HasForeignKey(d => d.Idklienta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Zakup_Klient");

                entity.HasOne(d => d.IdofertyNavigation)
                    .WithMany(p => p.Zakup)
                    .HasForeignKey(d => d.Idoferty)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Zakup_Oferta");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
