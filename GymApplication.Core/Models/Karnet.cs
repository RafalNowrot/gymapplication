﻿using System;
using System.Collections.Generic;

namespace GymApplication.Core.Models
{
    public partial class Karnet
    {
        public int Idkarnetu { get; set; }
        public DateTime DataZakupu { get; set; }
        public DateTime DataWaznosci { get; set; }
        public int Idoferty { get; set; }
        public int Idklienta { get; set; }

        public virtual Klient IdklientaNavigation { get; set; }
        public virtual Oferta IdofertyNavigation { get; set; }
    }
}
