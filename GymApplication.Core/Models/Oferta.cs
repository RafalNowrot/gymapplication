﻿using System;
using System.Collections.Generic;

namespace GymApplication.Core.Models
{
    public partial class Oferta
    {
        public Oferta()
        {
            Karnet = new HashSet<Karnet>();
            Zakup = new HashSet<Zakup>();
        }

        public int Idoferty { get; set; }
        public string NazwaOferty { get; set; }
        public decimal Cena { get; set; }
        public int MinGodzinaWejscia { get; set; }
        public int MaxGodzinaWejscia { get; set; }
        public int CzasTrwania { get; set; }
        public string Opis { get; set; }

        public virtual ICollection<Karnet> Karnet { get; set; }
        public virtual ICollection<Zakup> Zakup { get; set; }
    }
}
