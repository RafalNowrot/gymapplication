﻿using System;
using System.Collections.Generic;

namespace GymApplication.Core.Models
{
    public partial class Zakup
    {
        public int Idzakupu { get; set; }
        public int Idoferty { get; set; }
        public int Idklienta { get; set; }
        public DateTime DataZakupu { get; set; }
        public decimal Cena { get; set; }

        public virtual Klient IdklientaNavigation { get; set; }
        public virtual Oferta IdofertyNavigation { get; set; }
    }
}
