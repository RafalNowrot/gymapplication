﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace GymApplication.Core.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Klient",
                columns: table => new
                {
                    IDKlienta = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    imie = table.Column<string>(maxLength: 40, nullable: false),
                    nazwisko = table.Column<string>(maxLength: 40, nullable: false),
                    pesel = table.Column<string>(fixedLength: true, maxLength: 11, nullable: true),
                    email = table.Column<string>(unicode: false, maxLength: 50, nullable: true),
                    numerTelefonu = table.Column<string>(unicode: false, fixedLength: true, maxLength: 12, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Klient", x => x.IDKlienta)
                        .Annotation("SqlServer:Clustered", false);
                });

            migrationBuilder.CreateTable(
                name: "Oferta",
                columns: table => new
                {
                    IDOferty = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nazwaOferty = table.Column<string>(maxLength: 30, nullable: false),
                    cena = table.Column<decimal>(type: "money", nullable: false),
                    minGodzinaWejscia = table.Column<int>(nullable: false),
                    maxGodzinaWejscia = table.Column<int>(nullable: false),
                    czasTrwania = table.Column<int>(nullable: false),
                    opis = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Oferta", x => x.IDOferty)
                        .Annotation("SqlServer:Clustered", false);
                });

            migrationBuilder.CreateTable(
                name: "HistoriaWejsc",
                columns: table => new
                {
                    IDWejscia = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IDKlienta = table.Column<int>(nullable: false),
                    data = table.Column<DateTime>(type: "datetime", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_dbo.HistoriaWejsc", x => x.IDWejscia)
                        .Annotation("SqlServer:Clustered", false);
                    table.ForeignKey(
                        name: "FK_dbo.HistoriaWejsc_Klient",
                        column: x => x.IDKlienta,
                        principalTable: "Klient",
                        principalColumn: "IDKlienta",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Karnet",
                columns: table => new
                {
                    IDKarnetu = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    dataZakupu = table.Column<DateTime>(type: "date", nullable: false),
                    dataWaznosci = table.Column<DateTime>(type: "date", nullable: false),
                    IDOferty = table.Column<int>(nullable: false),
                    IDKlienta = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Karnet", x => x.IDKarnetu)
                        .Annotation("SqlServer:Clustered", false);
                    table.ForeignKey(
                        name: "FK_Karnet_Klient",
                        column: x => x.IDKlienta,
                        principalTable: "Klient",
                        principalColumn: "IDKlienta",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Karnet_Oferta",
                        column: x => x.IDOferty,
                        principalTable: "Oferta",
                        principalColumn: "IDOferty",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Zakup",
                columns: table => new
                {
                    IDZakupu = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    IDOferty = table.Column<int>(nullable: false),
                    IDKlienta = table.Column<int>(nullable: false),
                    dataZakupu = table.Column<DateTime>(type: "date", nullable: false),
                    cena = table.Column<decimal>(type: "money", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Zakup", x => x.IDZakupu)
                        .Annotation("SqlServer:Clustered", false);
                    table.ForeignKey(
                        name: "FK_Zakup_Klient",
                        column: x => x.IDKlienta,
                        principalTable: "Klient",
                        principalColumn: "IDKlienta",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Zakup_Oferta",
                        column: x => x.IDOferty,
                        principalTable: "Oferta",
                        principalColumn: "IDOferty",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_HistoriaWejsc_IDKlienta",
                table: "HistoriaWejsc",
                column: "IDKlienta");

            migrationBuilder.CreateIndex(
                name: "IX_Karnet_IDKlienta",
                table: "Karnet",
                column: "IDKlienta");

            migrationBuilder.CreateIndex(
                name: "IX_Karnet_IDOferty",
                table: "Karnet",
                column: "IDOferty");

            migrationBuilder.CreateIndex(
                name: "IX_Zakup_IDKlienta",
                table: "Zakup",
                column: "IDKlienta");

            migrationBuilder.CreateIndex(
                name: "IX_Zakup_IDOferty",
                table: "Zakup",
                column: "IDOferty");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "HistoriaWejsc");

            migrationBuilder.DropTable(
                name: "Karnet");

            migrationBuilder.DropTable(
                name: "Zakup");

            migrationBuilder.DropTable(
                name: "Klient");

            migrationBuilder.DropTable(
                name: "Oferta");
        }
    }
}
